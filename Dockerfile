FROM python:3.7


WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install -i https://mirrors.aliyun.com/pypi/simple/ --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "manage.py", "runserver", "0:8000"]
